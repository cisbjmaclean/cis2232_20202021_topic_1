package info.hccis.teetimebooker;

import info.hccis.teetimebooker.bo.Booking;
import com.google.gson.Gson;
import info.hccis.util.CisUtility;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Controller class
 *
 * @author bjm
 * @since 20200514
 */
public class Controller {

    public static final String EXIT = "X";
    public static final String PATH = "/bookings"+File.separator;
    public static final String FILE_NAME = "bookingsRandom.json";

    private static ArrayList<Booking> bookings = new ArrayList();

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Add a booking\n"
            + "- B-Show bookings\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    public static void main(String[] args) {

        setupFile();

        loadBookings(bookings);

        //Add a loop below to continuously promput the user for their choice 
        //until they choose to exit.
        String option = "";

        CisUtility.display("Tee Time Booker"
                + "\nToday is: " + CisUtility.getCurrentDate(null));
        do {
            option = CisUtility.getInputString(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase(EXIT));

    }

    /**
     * Load the bookings into the bookings ArrayList
     *
     * @since 2020-05-19
     * @author BJM
     */
    public static void loadBookings(ArrayList<Booking> bookings) {

        bookings.clear();
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = new RandomAccessFile(PATH + FILE_NAME, "r");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        byte[] dest = new byte[Booking.RECORD_LENGTH];
        int offset = 0;
        int length = Booking.RECORD_LENGTH;
        String jsonForBooking = null;
        Gson gson = new Gson();
        for (int i = 1; i <= Booking.MAX_RECORDS; i++) {
            try {
                randomAccessFile.seek(Booking.RECORD_LENGTH * (i - 1));
                int bytesRead = randomAccessFile.read(dest, offset, length);
                jsonForBooking = new String(dest);
                jsonForBooking.trim();
                if (!jsonForBooking.isEmpty() && jsonForBooking.startsWith("{")) {
                    //TODO figure out why it's not empty if not json
                    Booking booking = gson.fromJson(jsonForBooking, Booking.class);
                    //Issue#1 Set the next id.
                    Booking.setNextId(booking.getId());
                    bookings.add(booking);
                }

            } catch (IOException ex) {
                Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
        //Add a switch to process the option
        switch (option.toUpperCase()) {
            case "A":
                CisUtility.display("Add a booking");
                addABooking();
                break;
            case "B":
                CisUtility.display("Here are the bookings");
                showBookings(bookings);
                break;
            case "C":
                CisUtility.display("User picked c");
                break;
            case "GV":
                CisUtility.display(CisUtility.getRandom());
                break;
            case "X":
                CisUtility.display("User picked x");
                break;
            default:
                CisUtility.display("Invalid entry");
        }
    }

    /**
     * Read the file and show all the bookings
     *
     * @since 20200514
     * @author BJM
     */
    public static void showBookings(ArrayList<Booking> bookings) {

        bookings.clear();
        loadBookings(bookings);

        for (Booking current : bookings) {
            CisUtility.display(current.toString());
        }

    }

    /**
     * Add a booking by writing to the file
     *
     * @since 20200514
     * @author BJM
     */
    public static void addABooking() {

        RandomAccessFile file = null;
        try {
            file = new RandomAccessFile(PATH + FILE_NAME, "rwd");
        } catch (FileNotFoundException ex) {
            System.out.println("Error file doesn't exist.");
            return;
        }

        try {
            Booking booking = new Booking();
            booking.setNextId(); //Set the id based on the next available id.

            booking.getInformtion();

            //Use GSON to get a json string encoding for a booking
            Gson gson = new Gson();
            String bookingJson = gson.toJson(booking);

            //Pad the string up to 200 bytes
            for (int i = bookingJson.length(); i < Booking.RECORD_LENGTH; i++) {
                bookingJson += " ";
            }
            System.out.println("bookingJson length=" + bookingJson.length());

            file.seek(Booking.RECORD_LENGTH * (booking.getId() - 1));

            file.write(bookingJson.getBytes());

//            //writer.write(booking.toCSV()+System.lineSeparator());
//            writer.close();
        } catch (Exception ex) {
            CisUtility.display("Error with file access");
        } finally {
            try {
                file.close();
            } catch (IOException ex) {
                CisUtility.display("Error closing file");
            }
        }
    }

    /**
     * Using java nio classes to ensure the bookings file is setup.
     *
     * @since 20200514
     * @author BJM
     */
    public static void setupFile() {
        File myFile;
        try {
            Path path = Paths.get(PATH);
            try {
                Files.createDirectories(path);
            } catch (IOException ex) {
                CisUtility.display("Error creating directory");
            }

            myFile = new File(PATH + FILE_NAME);
            if (myFile.createNewFile()) {
                System.out.println("File is created!");
                //initialize with empty spaces (100 records with 200 bytes each)
                BufferedWriter writer = null;
                writer = new BufferedWriter(new FileWriter(PATH + FILE_NAME, false));
                for (int i = 0; i < Booking.MAX_RECORDS; i++) {
                    for (int j = 0; j < Booking.RECORD_LENGTH; j++) {
                        writer.write(" ");
                        //System.out.println("wrote a space");
                    }
                }
                writer.close();
            } else {
                System.out.println("File already exists.");
            }

        } catch (IOException ex) {
            CisUtility.display("Error creating new file");
        }
    }
}
