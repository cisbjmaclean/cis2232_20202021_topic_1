package cis.bo;

import com.google.gson.Gson;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author leck
 */
public class CamperBO {

    public ArrayList<Camper> load() {
        ArrayList<Camper> campers = new ArrayList();

        //read all lines from file
        Path path = Paths.get(Camper.FILE_LOCATION);
        try {
            List<String> allLines = Files.readAllLines(path);
            //loop thru each line and create camper object
            Gson gson = new Gson();
            for (String current : allLines) {
                Camper camper = gson.fromJson(current, Camper.class);

                //add camper to master list
                campers.add(camper);
            }
        } catch (Exception e) {
            System.out.println("Not able to load campers from file.");
        }

        return campers;
    }

}
